**Задача 2**
```
Изменить конфигурацию (из задачи 1) таким образом, чтобы была возможность задавать базовый префикс приложения через параметр APP_BASE_PREFIX (ARG).
Так, например, в случае если APP_BASE_PREFIX = /app/spa, то после сборки образа и запуска контейнера, 
все ресурсы должны быть доступны по ссылке http://localhost:3000/app/spa/*
По умолчанию, если параметр APP_BASE_PREFIX не передался, приложение также доступно по корневой ссылке.
```

app/ directory have index.html for test.

for demonize replace --rm on -d in docker run commands (all containers are automatically deleted after stop)

for build without ARG use:
```
docker build . -t ex2
docker run -p 3000:3000 --rm ex2
```

for build with ARG use (replace "/app/spa" on you ARG):
```
docker build --build-arg APP_BASE_PREFIX="/app/spa" . -t ex2
docker run -p 3000:3000 --rm ex2
```
