**Задача 1**
```
Создать конфигурацию для построения контейнера (Dockerfile, и т.д.), при помощи которой можно построить образ для хостинга
статичныхресурсов по http, например SPA приложение.

Вводные
- использовать базовый образ nginx:latest
- файлы (ресурсы) ожидаются в папке ./app
- приложение доступно на порту 3000, т.е., например, по ссылке http://localhost:3000 после запуска контейнера.
- никакой магии, просто отдаем статику

Ресурсы приложения после запуска должны быть доступны по ссылке http://localhost:3000/*
```

app/ directory have index.html for test.

for demonize replace --rm on -d in docker run commands (all containers are automatically deleted after stop)

```
docker build . -t ex1
```
```
docker run -p 3000:3000 --rm ex1
```

