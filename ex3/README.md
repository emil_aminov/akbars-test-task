**Задача 3 (опционально)**
```
Аналогично, только теперь эта переменная может изменяться в окружении запущенного контейнера, а не только при сборке.
При сборке лишь задается значение по умолчанию. Т.е. должна быть возможность запускать один и тот же полученный образ 
с разными значениями APP_BASE_PREFIX.
```

app/ directory have index.html for test.

for demonize replace --rm on -d in docker run commands (all containers are automatically deleted after stop)

for build with default location use:
```
docker build . -t ex3
```

for build with custom location use ARG (change /app/spa on you value):
```
docker build --build-arg APP_BASE_PREFIX="/app/spa" . -t ex3
```

for running with default location
```
docker run -p 3000:3000 --rm ex3
```

for running with custom location (change /app/spa on you value):
```
docker run -p 3000:3000 -e APP_BASE_PREFIX=/app/spa --rm ex3
```
